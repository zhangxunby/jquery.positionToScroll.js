

div位置随滚动条位置在一定范围内水平移动，控制fixed定位和绝对定位 


方法 $('#element').positionToScroll({starty:300,desty:2200,movedist:1200}); 
starty为滚动条触发div移动的初始位置 
desty为滚动条结束触发div移动的位置 
movedist为水平移动的位置
