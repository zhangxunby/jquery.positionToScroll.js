//written by rex zhou
//any question email 49158036@qq.com
//ver 1.0
(function($) {    
$.fn.positionToScroll = function ( options ) {

	var defaults = {    
    starty: 0,    
    desty: 100,   
    movedist:100
    };
    var opts = $.extend(defaults, options);   

    var $this = this,
    c = $(this).height(),
    initaly=parseInt($(this).css('top')),
    $window = $(window);
    //initalize
    $this.css({position:'absolute'})
    
    $window.scroll(function(e){
    	var s = $window.scrollTop();
    	console.log(s)
    	if(s>=opts.desty){
    		$this.css('position','absolute')
    		$this.css('top',opts.desty+$this.width());

    	}else if(s<=opts.starty){
    		$this.css('position','absolute')
    		$this.css('top',initaly);

    	}else{
    		$this.css('position','fixed')
    		$this.css('top',initaly-opts.starty);
    		scrollPercent = ((s-opts.starty) / (opts.movedist - c));
    		if(scrollPercent>1){
		        scrollPercent=1
		    }

			var position = (scrollPercent * ($(document).width()/2 - $this.width()/2));   
			$this.css({'left': position});
    	}
       
    });

    function debug($obj) {    
    
        
    };   

	return this.each(function() {   


	});
};
})(jQuery); 